#!/usr/bin/env python
# coding: utf-8

# # Exemplo de código para solução do modelo SIR
# ### Premissas:
# - Sem demografia
# - População homogênea
# 
# ### Sistema de equações diferenciais:
# Denotando as frações de indivíduos suscetíveis, infectados e removidos por $S$, $I$ e $R$, respectivamente. Considerando uma taxa de propagação $\lambda$ e uma taxa de recuperação $\mu$ o sistema de equações diferenciais que descreve a evolução temporal desta dinâmica é dado por:
# 
# $$\dfrac{dS}{dt} = -\lambda S I + \mu I$$
# 
# $$\dfrac{dI}{dt} = \lambda S I - \mu I$$
# 
# ### Comentário:
# Os parâmetros utilizados nestes examplos foram escolhidos unicamente para facilitar a visualização.

# In[26]:


# Importando as bibliotecas necessárias

import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot as plt

plt.rcParams.update({'font.size': 26})  # increase the font size


# ### Função que define o sistema de equações diferenciais 

# In[27]:


def SIS (State, t):
    # State = [S, I]
        
    S = -lambdaRate*State[0]*State[1] +  muRate*State[1];
    I = lambdaRate*State[0]*State[1] - muRate*State[1];
    
    return [S, I];


# ### Primeiro exemplo: parâmetros constantes no tempo
# Definição dos parâmetros, condições iniciais e resolução do sistema

# In[28]:


# Parameters
lambdaRate = 5;
muRate = 1.0;

# Initial conditions
I_0 = 0.01;
S_0 = 1.0 - I_0;
y_0 = [S_0, I_0];

# time
t = np.linspace(0,7,1000)

yo = odeint(SIS, y_0, t);


# Plotando os resultados:
# 
# Uma vez que $S+I+R = 1$ a curva de suscetíveis não foi mostrada.

# In[29]:


plt.figure(figsize=(14, 8));
# plt.plot(t, yo[:,0], label=r'$S$')
plt.plot(t, yo[:,1], label=r'$I$')
plt.legend(frameon=False);

plt.xlabel(r'$t$');
plt.ylabel(r'$\rho$');

# Saving figure
plt.tight_layout();
plt.savefig("SIS_0.pdf")


# In[30]:


# Initial conditions
I_0 = 0.01;
S_0 = 1.0 - I_0;
y_0 = [S_0, I_0];

# time
t = np.linspace(0,7,1000)
t2 = 1.;
t3 = 2;


# Parameters
lambdaRate = 5;
muRate = 1.0;

y1 = odeint(SIS, y_0, t[t<t2]);

# Parameters
lambdaRate = 0.5;
muRate = 1.0;

y2 = odeint(SIS, y1[-1,:], t[np.logical_and(t>=t2, t<t3)]);

# Parameters
lambdaRate = 5;
muRate = 1.0;

y3 = odeint(SIS, y2[-1,:], t[t>=t3]);

y = np.vstack((y1,y2, y3));

plt.figure(figsize=(14, 8));
# plt.plot(t, y[:,0], label=r'$S$')
plt.plot(t, y[:,1], label=r'$I$')
plt.legend(frameon=False);

plt.xlabel(r'$t$');
plt.ylabel(r'$\rho$');

# Saving figure
plt.tight_layout();
plt.savefig("SIS_1.pdf")


# In[ ]:




